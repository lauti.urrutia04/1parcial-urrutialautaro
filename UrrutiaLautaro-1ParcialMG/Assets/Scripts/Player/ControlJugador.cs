using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    //movement
    public float velocidadBase = 15f;
    public float rapidezDesplazamiento = 15f;
    //salto
    public Rigidbody rb;
    public float saltoBase = 6f;
    public float saltoVel = 6f;
    private bool enElSuelo = true;
    public int maxSaltos = 2;
    private int saltoActual = 0;
    //disparo
    public Camera camaraPrimeraPersona;
    public GameObject proyectil;
    float Disparo;
    float FireRate = 0.5f;
    //vida
    public int hp;
    private int BaseHp = 100;
    public GameObject outOfBounds;
    //Coleccionables
    private int powerups;
    private int goldCount;
    //UI
    public TMPro.TMP_Text TextoVida;
    public TMPro.TMP_Text TextoPowerUps;
    public TMPro.TMP_Text TextoOro;
    public TMPro.TMP_Text TextoPerdiste;
    public TMPro.TMP_Text TextoGanaste;

    public GameObject efecto;

    public Light luz;

    void Start() 
    {
        hp = BaseHp;

        powerups = 5;
        goldCount = 0;
        
        rb = GetComponent<Rigidbody>();

        SetearTextos();
    }
    
    void Update()
    {
        Cursor.lockState = CursorLockMode.Locked;

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;
        
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (hp > 0)
        {
            TextoPerdiste.text = "";
        }
        else
        {
            Time.timeScale = 0;
            SetearTextos();
        }

        if (Input.GetButtonDown("Jump") && (enElSuelo || maxSaltos > saltoActual))
        {
            rb.velocity = new Vector3(0f, saltoVel, 0f * Time.deltaTime);
            enElSuelo = false;
            saltoActual++;
        }

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Disparo > 0)
        {
            Disparo -= Time.deltaTime;
        }

        if (Input.GetMouseButtonDown(0))
        {   if (Disparo <= 0)
            {
                Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                GameObject pro;
                pro = Instantiate(proyectil, ray.origin, transform.rotation);

                Rigidbody rb = pro.GetComponent<Rigidbody>();
                rb.AddForce(camaraPrimeraPersona.transform.forward * 40, ForceMode.Impulse);

                Destroy(pro, 2);
                Disparo = FireRate;

                GestorDeAudio.instancia.ReproducirSonido("Latigazo");
            }
            else
            {
                GestorDeAudio.instancia.ReproducirSonido("EmptyMag");
            }            
        }

        if (Input.GetKeyDown("r"))
        {
            Time.timeScale = 1;
            luz.enabled = true;
            Restart(); 
        }

        if (Input.GetKeyDown("f"))
        {
            luz.enabled = !luz.enabled;
        }
    }

    private void SetearTextos()
    {
        SetearTextoVida();
        SetearTextoPowerUp();
        SetearTextoGold();
        SetearTextoPerdiste();
        SetearTextoGanaste();
    }

    private void SetearTextoVida()
    {
        TextoVida.text = "HP: " + hp;
    }

    private void SetearTextoPowerUp()
    {
        TextoPowerUps.text = "Mejoras sueltas: " + powerups.ToString();
        if (powerups <= 0)
        {
            TextoPowerUps.text = "Bien hecho! Ahora a robar el oro encima de las plataformas";
        }
    }

    private void SetearTextoGold()
    {
        TextoOro.text = "";
        if (powerups <= 0)
        {
            TextoOro.text = "Oro recogido: " + goldCount.ToString() + "/5";
        }
    }

    private void SetearTextoPerdiste()
    {
        TextoPerdiste.text = "";
        if (hp <= 0)
        {
            TextoPerdiste.text = "Te quedaste sin vida, apreta R para reiniciar";
        }
    }

    private void SetearTextoGanaste()
    {
        TextoGanaste.text = "";
        if (goldCount >= 5)
        {
            Time.timeScale = 0;
            TextoGanaste.text = "�Enhorabuena! �Conseguiste todo el oro!";
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        //salto
        if (collision.gameObject.CompareTag("Piso"))
        {
            enElSuelo = true;
            saltoActual = 0;
        }
        //da�o
        if (collision.gameObject.CompareTag("Crawler"))
        {
            CrawlerRecibirDa�o();          
            collision.gameObject.SetActive(false); 
        }

        if (collision.gameObject.CompareTag("caBullet"))
        {
            TurretRecibirDa�o();
            collision.gameObject.SetActive(false);
        }


        //PowerUps
        if (collision.gameObject.CompareTag("PowerUp"))
        {
            collision.gameObject.SetActive(false);
            rapidezDesplazamiento = rapidezDesplazamiento * 1.1f;
            hp = hp + 10;
            saltoVel = saltoVel * 1.15f;
            powerups = powerups - 1;
            GameObject particulas = Instantiate(efecto, transform.position, Quaternion.identity) as GameObject;
            SetearTextos();
        }

        if (collision.gameObject.CompareTag("Gold"))
        {
            collision.gameObject.SetActive(false);
            goldCount = goldCount + 1;
            SetearTextos();
        }

        if (collision.gameObject.CompareTag("outOfBounds"))
        {
            Restart();
        }
    }

    public void CrawlerRecibirDa�o()
    {
        hp = hp - 10;
        SetearTextos();
    }

    public void TurretRecibirDa�o()
    {
        hp = hp - 20;
        SetearTextos();
    }

    private void Restart()
    {
        hp = BaseHp;
        rapidezDesplazamiento = velocidadBase;
        saltoVel = saltoBase;
        powerups = 5;
        goldCount = 0;
        SetearTextos();
    }
}