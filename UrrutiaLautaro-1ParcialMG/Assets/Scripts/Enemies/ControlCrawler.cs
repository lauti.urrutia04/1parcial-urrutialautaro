using UnityEngine;

public class ControlCrawler : MonoBehaviour
{
    private int hp;
    public GameObject jugador;
    public int rapidez;
    
    void Start()
    {
        hp = 50;
        jugador = GameObject.Find("Jugador");
    }

    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);

        //GestorDeAudio.instancia.ReproducirSonido("Crawl");
    }
    
    public void recibirDaņo()
    {
        hp = hp - 25;
        if (hp <= 0)
        { 
            this.desaparecer();
        }
    }
    
    private void desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("plBullet"))
        {
            recibirDaņo();
        }

        if (other.gameObject.CompareTag("plBullet") == true)
        {
            other.gameObject.SetActive(false);
        }
    }
}