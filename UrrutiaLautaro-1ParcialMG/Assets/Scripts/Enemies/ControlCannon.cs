using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCannon : MonoBehaviour
{
    private int hp;
    public GameObject jugador;
    public GameObject gunpoint;
    public GameObject proyectil;
    public Vector3 offset;
    float fire;
    float delay = 0.75f;

    void Start()
    {
        hp = 75;
        jugador = GameObject.Find("Jugador");
        offset = transform.position;
    }

    public void Update()
    {
        transform.LookAt(jugador.transform);

        if (fire > 0)
        {
            fire -= Time.deltaTime;
        }
     
        if (fire <= 0)
        {
            GameObject pro;
            pro = Instantiate(proyectil, offset, Quaternion.identity);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(this.transform.forward * 30, ForceMode.Impulse);

            Destroy(pro, 1);
            fire = delay;
        }
        
    }



    public void recibirDaņo()
    {
        hp = hp - 25;
        if (hp <= 0)
        {
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("plBullet"))
        {
            recibirDaņo();
        }

        if (other.gameObject.CompareTag("plBullet") == true)
        {
            other.gameObject.SetActive(false);
        }
    }
}
