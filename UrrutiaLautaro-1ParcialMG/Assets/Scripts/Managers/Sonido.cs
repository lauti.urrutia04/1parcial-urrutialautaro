using UnityEngine;
[System.Serializable] 

public class Sonido 
{ 
    public string Nombre; 
    public AudioClip ClipSonido;

    [Range(0f, 1f)] 
    public float Volumen;

    [Range(0f, 2f)] 
    public float pitch;

    [Range(0f, 1f)]
    public float dopplerLevel; //no funcion�

    [Range(0f, 1f)]
    public float spatialBlend;

    public float minDistance;
    public float maxDistance; // no funcionaron

    public bool repetir;

    [HideInInspector] 
    public AudioSource FuenteAudio; 
}
