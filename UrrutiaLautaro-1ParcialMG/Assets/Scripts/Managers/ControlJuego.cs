using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJuego : MonoBehaviour
{
    public GameObject jugador;
    public GameObject crawler;
    public GameObject turret;
    public GameObject powerup;
    public GameObject gold;
    private List<GameObject> listaEnemigos = new List<GameObject>();
    private List<GameObject> listaBuffs = new List<GameObject>();
    public GameObject OOB;

    int timerBaseValue = 180;
    int timer;

    public TMPro.TMP_Text textoTimer;
    public TMPro.TMP_Text textoPerdiste;

    void Start()
    {
        ComenzarJuego();
    }

    void Update()
    {
        if (timer > 0)
        {
            textoPerdiste.text = "";
            textoTimer.text = "Timer: " + timer;
        }
        else
        {
            textoTimer.text = "no time";    
            textoPerdiste.text = "Te quedaste sin tiempo, apreta R para reiniciar";
            Time.timeScale = 0;
        }

        if (Input.GetKeyDown("r"))
        {
            CancelInvoke("TimerCountdown");
            ComenzarJuego();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            CancelInvoke("TimerCountdown");
            ComenzarJuego();
        }
    }

    void ComenzarJuego()
    {
        Time.timeScale = 1;
        timer = timerBaseValue;
        jugador.transform.position = new Vector3(-2f, 0.5f, -4f);

        foreach (GameObject item in listaEnemigos)
        {
            Destroy(item);
        }

        foreach (GameObject item in listaBuffs)
        {
            Destroy(item);
        }

        listaEnemigos.Add(Instantiate(crawler, new Vector3(0f, 0f, 25f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(crawler, new Vector3(-2f, 0f, 25f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(crawler, new Vector3(-17f, 0f, -27f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(crawler, new Vector3(-18f, 0f, -26f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(crawler, new Vector3(-56f, 0f, 30f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(crawler, new Vector3(-57f, 0f, 29f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(crawler, new Vector3(30f, 0f, 53f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(crawler, new Vector3(29f, 0f, 52f), Quaternion.identity));

        listaEnemigos.Add(Instantiate(turret, new Vector3(7f, 6.5f, 71f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(turret, new Vector3(33f, 34f, 30f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(turret, new Vector3(33f, 34f, 50f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(turret, new Vector3(10f, 44f, -11f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(turret, new Vector3(30f, 44f, -11f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(turret, new Vector3(-30f, 54f, 10f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(turret, new Vector3(-30f, 54f, -10f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(turret, new Vector3(0f, 64f, 30f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(turret, new Vector3(5f, 64f, 30f), Quaternion.identity));
        //listaEnemigos.Add(Instantiate(turret, new Vector3(0f, 64f, 10f), Quaternion.identity));

        listaBuffs.Add(Instantiate(powerup, new Vector3(15f, 1f, -12f), Quaternion.identity));
        listaBuffs.Add(Instantiate(powerup, new Vector3(12f, 1f, 19f), Quaternion.identity));
        listaBuffs.Add(Instantiate(powerup, new Vector3(15f, 1f, 70f), Quaternion.identity));
        listaBuffs.Add(Instantiate(powerup, new Vector3(-40f, 1f, 12f), Quaternion.identity));
        listaBuffs.Add(Instantiate(powerup, new Vector3(-60f, 1f, 40f), Quaternion.identity));

        listaBuffs.Add(Instantiate(gold, new Vector3(-20f, 21f, 40f), Quaternion.identity));
        listaBuffs.Add(Instantiate(gold, new Vector3(20f, 31f, 40f), Quaternion.identity));
        listaBuffs.Add(Instantiate(gold, new Vector3(20f, 41f, 0f), Quaternion.identity));
        listaBuffs.Add(Instantiate(gold, new Vector3(-20f, 51f, 0f), Quaternion.identity));
        listaBuffs.Add(Instantiate(gold, new Vector3(0f, 61f, 20f), Quaternion.identity));

        TimerCountdown();
    }

    void TimerCountdown()
    {       
        if (timer > 0)
        {
            timer--;
            Invoke("TimerCountdown", 1);  
        }
    }
}