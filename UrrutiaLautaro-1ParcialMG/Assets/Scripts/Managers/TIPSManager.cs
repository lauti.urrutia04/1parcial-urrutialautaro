using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TIPSManager : MonoBehaviour
{
    public TMPro.TMP_Text textoTips;
    public float conteoDeTips;
    float tipTimer;
    float baseTipTimer = 7f;

    void tipUno()
    {
        textoTips.text = "Aprieta click izquierdo para disparar";
        conteoDeTips = 1f;
        tipTimer = baseTipTimer;
    }
    
    void Start()
    {
        tipUno();
    }

    void Update()
    {
        if (Input.GetKeyDown("r"))
        {
            tipUno();
        }

        if (conteoDeTips == 1f & Input.GetMouseButtonDown(0))
        {
            tipDos();
        }

        if (conteoDeTips == 2f & Input.GetKeyDown("f"))
        {
            tipTres();
        }

        if (conteoDeTips == 3f & Input.GetButtonDown("Jump"))
        {
            tipCuatro();
        }

        if (tipTimer <= 0f)
        {
            CancelInvoke("CountdownTips");
            textoTips.text = "";
        }
    }

    void tipDos()
    {
        textoTips.text = "Aprieta 'F' para prender y apagar la linterna";
        conteoDeTips++;
    }

    void tipTres()
    {
        textoTips.text = "Aprieta Espacio dos veces para salto y doble salto";
        conteoDeTips++;
    }

    void tipCuatro()
    {
        textoTips.text = "Ahora, pasa por las mejoras del primer piso para agarrarlas";
        CountdownTips();
    }

    void CountdownTips()
    {
        if (tipTimer > 0f)
        {
            tipTimer--;
            Invoke("CountdownTips", 1);
        }
    }
}
