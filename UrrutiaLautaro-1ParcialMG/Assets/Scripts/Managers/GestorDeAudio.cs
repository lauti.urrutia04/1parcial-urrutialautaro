using System;
using UnityEngine;

public class GestorDeAudio : MonoBehaviour
{
    public Sonido[] sonidos;
    public static GestorDeAudio instancia;
    public enum audioSourceCurveType { linearRolloff };
    public audioSourceCurveType Curva;

    void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Sonido s in sonidos)
        {
            s.FuenteAudio = gameObject.AddComponent<AudioSource>();
            s.FuenteAudio.clip = s.ClipSonido;
            s.FuenteAudio.volume = s.Volumen;
            s.FuenteAudio.pitch = s.pitch;
            s.FuenteAudio.dopplerLevel = s.dopplerLevel;
            s.FuenteAudio.spatialBlend = s.spatialBlend;
            s.FuenteAudio.minDistance = s.minDistance;
            s.FuenteAudio.maxDistance = s.maxDistance;
            s.FuenteAudio.loop = s.repetir;
        }
    }

    public void ReproducirSonido(string nombre)
    {
        Sonido s = Array.Find(sonidos, sound => sound.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Play();
        }
    }

    float EvaluarDistancia()
    {
        float x = 1; //provisorio
        float y;

        switch (Curva)
        {
            case audioSourceCurveType.linearRolloff:
                y = x * -2f + 20;
                break;
            default:
                y = 1f;
                break;
        }
        return y * 1f;
    }
}